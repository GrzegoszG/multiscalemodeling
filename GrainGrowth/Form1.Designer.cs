﻿namespace GrainGrowth
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pb = new System.Windows.Forms.PictureBox();
            this.panelPicture = new System.Windows.Forms.Panel();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lblX = new System.Windows.Forms.Label();
            this.lblY = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnTransformStructure = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.cbStructure = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbPeriodical = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.trackBar3 = new System.Windows.Forms.TrackBar();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnTransform = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.nudBoundSize = new System.Windows.Forms.NumericUpDown();
            this.cbShowBoundaries = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbGB = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAddInclusions = new System.Windows.Forms.Button();
            this.cbInclusions = new System.Windows.Forms.CheckBox();
            this.cbInclusionShape = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gbMC = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btnMCSeed = new System.Windows.Forms.Button();
            this.btnMCIteration = new System.Windows.Forms.Button();
            this.cbMCDP = new System.Windows.Forms.CheckBox();
            this.btnMCSimulation = new System.Windows.Forms.Button();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.btnRSX = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.nudRSXIterations = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.cbRSXLocation = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.nudRSXIncrease = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.nudRSXNucleons = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.cbRsx = new System.Windows.Forms.ComboBox();
            this.gbShowEnergy = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.nudJ = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.nudEnergy2 = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.nudEnergy = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.cbEnergyDistribution = new System.Windows.Forms.ComboBox();
            this.cbShowEnergy = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnDistributeEnergy = new System.Windows.Forms.Button();
            this.nudEnergyColors = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).BeginInit();
            this.panelPicture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBoundSize)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.gbMC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRSXIterations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRSXIncrease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRSXNucleons)).BeginInit();
            this.gbShowEnergy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEnergy2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEnergy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEnergyColors)).BeginInit();
            this.SuspendLayout();
            // 
            // pb
            // 
            this.pb.BackColor = System.Drawing.Color.White;
            this.pb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb.Location = new System.Drawing.Point(353, 276);
            this.pb.Margin = new System.Windows.Forms.Padding(4);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(212, 229);
            this.pb.TabIndex = 0;
            this.pb.TabStop = false;
            this.pb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pb_MouseClick);
            // 
            // panelPicture
            // 
            this.panelPicture.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelPicture.BackColor = System.Drawing.Color.White;
            this.panelPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPicture.Controls.Add(this.pb);
            this.panelPicture.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelPicture.Location = new System.Drawing.Point(0, 28);
            this.panelPicture.Margin = new System.Windows.Forms.Padding(4);
            this.panelPicture.Name = "panelPicture";
            this.panelPicture.Size = new System.Drawing.Size(861, 845);
            this.panelPicture.TabIndex = 1;
            this.panelPicture.SizeChanged += new System.EventHandler(this.panelPicture_SizeChanged);
            // 
            // trackBar1
            // 
            this.trackBar1.LargeChange = 1;
            this.trackBar1.Location = new System.Drawing.Point(9, 31);
            this.trackBar1.Margin = new System.Windows.Forms.Padding(4);
            this.trackBar1.Maximum = 500;
            this.trackBar1.Minimum = 100;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(139, 56);
            this.trackBar1.TabIndex = 2;
            this.trackBar1.Value = 100;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // trackBar2
            // 
            this.trackBar2.LargeChange = 1;
            this.trackBar2.Location = new System.Drawing.Point(198, 31);
            this.trackBar2.Margin = new System.Windows.Forms.Padding(4);
            this.trackBar2.Maximum = 500;
            this.trackBar2.Minimum = 100;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(139, 56);
            this.trackBar2.TabIndex = 3;
            this.trackBar2.Value = 100;
            this.trackBar2.Scroll += new System.EventHandler(this.trackBar2_Scroll);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(83, 88);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(119, 22);
            this.numericUpDown1.TabIndex = 4;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(14, 688);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(325, 44);
            this.button1.TabIndex = 5;
            this.button1.Text = "Simulate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1439, 28);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem,
            this.exportToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.fileToolStripMenuItem.Text = "File...";
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(138, 26);
            this.importToolStripMenuItem.Text = "Import...";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.importToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.textFileToolStripMenuItem,
            this.imageToolStripMenuItem});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(138, 26);
            this.exportToolStripMenuItem.Text = "Export...";
            // 
            // textFileToolStripMenuItem
            // 
            this.textFileToolStripMenuItem.Name = "textFileToolStripMenuItem";
            this.textFileToolStripMenuItem.Size = new System.Drawing.Size(138, 26);
            this.textFileToolStripMenuItem.Text = "Text File";
            this.textFileToolStripMenuItem.Click += new System.EventHandler(this.textFileToolStripMenuItem_Click);
            // 
            // imageToolStripMenuItem
            // 
            this.imageToolStripMenuItem.Name = "imageToolStripMenuItem";
            this.imageToolStripMenuItem.Size = new System.Drawing.Size(138, 26);
            this.imageToolStripMenuItem.Text = "Image";
            this.imageToolStripMenuItem.Click += new System.EventHandler(this.imageToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Size X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(195, 10);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Size Y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 89);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Nucleons";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(14, 739);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(325, 23);
            this.progressBar1.TabIndex = 10;
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.Location = new System.Drawing.Point(82, 16);
            this.lblX.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(0, 17);
            this.lblX.TabIndex = 11;
            // 
            // lblY
            // 
            this.lblY.AutoSize = true;
            this.lblY.Location = new System.Drawing.Point(270, 16);
            this.lblY.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(0, 17);
            this.lblY.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 765);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(274, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "Finished! Now you can export your results.";
            this.label4.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(863, 28);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(576, 845);
            this.panel2.TabIndex = 14;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(573, 830);
            this.tabControl1.TabIndex = 34;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(565, 801);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "CA";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.White;
            this.groupBox5.Controls.Add(this.btnClear);
            this.groupBox5.Controls.Add(this.button2);
            this.groupBox5.Controls.Add(this.groupBox4);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.groupBox3);
            this.groupBox5.Controls.Add(this.lblX);
            this.groupBox5.Controls.Add(this.groupBox2);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.numericUpDown1);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.progressBar1);
            this.groupBox5.Controls.Add(this.lblY);
            this.groupBox5.Controls.Add(this.button1);
            this.groupBox5.Controls.Add(this.trackBar1);
            this.groupBox5.Controls.Add(this.groupBox1);
            this.groupBox5.Controls.Add(this.trackBar2);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(560, 795);
            this.groupBox5.TabIndex = 34;
            this.groupBox5.TabStop = false;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(362, 688);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(171, 44);
            this.btnClear.TabIndex = 29;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(227, 82);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(191, 32);
            this.button2.TabIndex = 30;
            this.button2.Text = "Distribute Nucleons";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnTransformStructure);
            this.groupBox4.Controls.Add(this.textBox1);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.cbStructure);
            this.groupBox4.Location = new System.Drawing.Point(0, 540);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(560, 141);
            this.groupBox4.TabIndex = 33;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Structure";
            // 
            // btnTransformStructure
            // 
            this.btnTransformStructure.Location = new System.Drawing.Point(126, 102);
            this.btnTransformStructure.Margin = new System.Windows.Forms.Padding(4);
            this.btnTransformStructure.Name = "btnTransformStructure";
            this.btnTransformStructure.Size = new System.Drawing.Size(325, 32);
            this.btnTransformStructure.TabIndex = 33;
            this.btnTransformStructure.Text = "Transform selected to a new structure";
            this.btnTransformStructure.UseVisualStyleBackColor = true;
            this.btnTransformStructure.Click += new System.EventHandler(this.btnTransformStructure_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(170, 73);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(77, 22);
            this.textBox1.TabIndex = 30;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 73);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(154, 17);
            this.label15.TabIndex = 29;
            this.label15.Text = "Selected Grains Count:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 30);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(152, 17);
            this.label14.TabIndex = 28;
            this.label14.Text = "Set selected grains as:";
            // 
            // cbStructure
            // 
            this.cbStructure.FormattingEnabled = true;
            this.cbStructure.Location = new System.Drawing.Point(170, 27);
            this.cbStructure.Name = "cbStructure";
            this.cbStructure.Size = new System.Drawing.Size(220, 24);
            this.cbStructure.TabIndex = 27;
            this.cbStructure.SelectedIndexChanged += new System.EventHandler(this.cbStructure_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbPeriodical);
            this.groupBox3.Controls.Add(this.checkBox1);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.trackBar3);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.comboBox1);
            this.groupBox3.Location = new System.Drawing.Point(0, 361);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(560, 164);
            this.groupBox3.TabIndex = 32;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Growth";
            // 
            // cbPeriodical
            // 
            this.cbPeriodical.AutoSize = true;
            this.cbPeriodical.Location = new System.Drawing.Point(9, 56);
            this.cbPeriodical.Name = "cbPeriodical";
            this.cbPeriodical.Size = new System.Drawing.Size(227, 21);
            this.cbPeriodical.TabIndex = 28;
            this.cbPeriodical.Text = "Periodical Boundary Conditions";
            this.cbPeriodical.UseVisualStyleBackColor = true;
            this.cbPeriodical.CheckedChanged += new System.EventHandler(this.cbPeriodical_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(9, 25);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(209, 21);
            this.checkBox1.TabIndex = 25;
            this.checkBox1.Text = "Use modified transition rules";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(299, 25);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 17);
            this.label9.TabIndex = 22;
            this.label9.Text = "Propability:";
            // 
            // trackBar3
            // 
            this.trackBar3.LargeChange = 10;
            this.trackBar3.Location = new System.Drawing.Point(377, 12);
            this.trackBar3.Margin = new System.Windows.Forms.Padding(4);
            this.trackBar3.Maximum = 100;
            this.trackBar3.Name = "trackBar3";
            this.trackBar3.Size = new System.Drawing.Size(139, 56);
            this.trackBar3.TabIndex = 23;
            this.trackBar3.Value = 100;
            this.trackBar3.Scroll += new System.EventHandler(this.trackBar3_Scroll);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(524, 25);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 17);
            this.label10.TabIndex = 24;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 86);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 17);
            this.label11.TabIndex = 27;
            this.label11.Text = "Neighbourhood:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(126, 83);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(220, 24);
            this.comboBox1.TabIndex = 26;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnTransform);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.nudBoundSize);
            this.groupBox2.Controls.Add(this.cbShowBoundaries);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.tbGB);
            this.groupBox2.Location = new System.Drawing.Point(0, 257);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(560, 98);
            this.groupBox2.TabIndex = 31;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Boundaries";
            // 
            // btnTransform
            // 
            this.btnTransform.Location = new System.Drawing.Point(126, 58);
            this.btnTransform.Margin = new System.Windows.Forms.Padding(4);
            this.btnTransform.Name = "btnTransform";
            this.btnTransform.Size = new System.Drawing.Size(325, 32);
            this.btnTransform.TabIndex = 32;
            this.btnTransform.Text = "Transform boundaries to a new structure";
            this.btnTransform.UseVisualStyleBackColor = true;
            this.btnTransform.Click += new System.EventHandler(this.btnTransform_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(195, 30);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 17);
            this.label13.TabIndex = 31;
            this.label13.Text = "Size:";
            // 
            // nudBoundSize
            // 
            this.nudBoundSize.Location = new System.Drawing.Point(239, 25);
            this.nudBoundSize.Margin = new System.Windows.Forms.Padding(4);
            this.nudBoundSize.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudBoundSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudBoundSize.Name = "nudBoundSize";
            this.nudBoundSize.Size = new System.Drawing.Size(103, 22);
            this.nudBoundSize.TabIndex = 30;
            this.nudBoundSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudBoundSize.ValueChanged += new System.EventHandler(this.nudBoundSize_ValueChanged);
            // 
            // cbShowBoundaries
            // 
            this.cbShowBoundaries.AutoSize = true;
            this.cbShowBoundaries.Enabled = false;
            this.cbShowBoundaries.Location = new System.Drawing.Point(14, 30);
            this.cbShowBoundaries.Name = "cbShowBoundaries";
            this.cbShowBoundaries.Size = new System.Drawing.Size(175, 21);
            this.cbShowBoundaries.TabIndex = 15;
            this.cbShowBoundaries.Text = "Show grain boundaries";
            this.cbShowBoundaries.UseVisualStyleBackColor = true;
            this.cbShowBoundaries.CheckedChanged += new System.EventHandler(this.cbShowBoundaries_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(359, 27);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 17);
            this.label12.TabIndex = 28;
            this.label12.Text = "GB percentage:";
            // 
            // tbGB
            // 
            this.tbGB.Location = new System.Drawing.Point(474, 25);
            this.tbGB.Name = "tbGB";
            this.tbGB.ReadOnly = true;
            this.tbGB.Size = new System.Drawing.Size(61, 22);
            this.tbGB.TabIndex = 29;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAddInclusions);
            this.groupBox1.Controls.Add(this.cbInclusions);
            this.groupBox1.Controls.Add(this.cbInclusionShape);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.numericUpDown3);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.numericUpDown2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(0, 121);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(560, 130);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Inclusions";
            // 
            // btnAddInclusions
            // 
            this.btnAddInclusions.Enabled = false;
            this.btnAddInclusions.Location = new System.Drawing.Point(126, 91);
            this.btnAddInclusions.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddInclusions.Name = "btnAddInclusions";
            this.btnAddInclusions.Size = new System.Drawing.Size(325, 32);
            this.btnAddInclusions.TabIndex = 33;
            this.btnAddInclusions.Text = "Add Inclusions";
            this.btnAddInclusions.UseVisualStyleBackColor = true;
            this.btnAddInclusions.Click += new System.EventHandler(this.btnAddInclusions_Click);
            // 
            // cbInclusions
            // 
            this.cbInclusions.AutoSize = true;
            this.cbInclusions.Location = new System.Drawing.Point(14, 21);
            this.cbInclusions.Name = "cbInclusions";
            this.cbInclusions.Size = new System.Drawing.Size(74, 21);
            this.cbInclusions.TabIndex = 25;
            this.cbInclusions.Text = "Enable";
            this.cbInclusions.UseVisualStyleBackColor = true;
            this.cbInclusions.CheckedChanged += new System.EventHandler(this.cbInclusions_CheckedChanged);
            // 
            // cbInclusionShape
            // 
            this.cbInclusionShape.FormattingEnabled = true;
            this.cbInclusionShape.Location = new System.Drawing.Point(75, 48);
            this.cbInclusionShape.Name = "cbInclusionShape";
            this.cbInclusionShape.Size = new System.Drawing.Size(103, 24);
            this.cbInclusionShape.TabIndex = 24;
            this.cbInclusionShape.SelectedIndexChanged += new System.EventHandler(this.cbInclusionShape_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(382, 50);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 17);
            this.label8.TabIndex = 23;
            this.label8.Text = "Size:";
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Location = new System.Drawing.Point(441, 50);
            this.numericUpDown3.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown3.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(94, 22);
            this.numericUpDown3.TabIndex = 22;
            this.numericUpDown3.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown3.ValueChanged += new System.EventHandler(this.numericUpDown3_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(198, 50);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 17);
            this.label7.TabIndex = 21;
            this.label7.Text = "Count:";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(255, 50);
            this.numericUpDown2.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(87, 22);
            this.numericUpDown2.TabIndex = 20;
            this.numericUpDown2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown2.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(187, 22);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 17);
            this.label5.TabIndex = 19;
            this.label5.Text = "Distribution:";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(272, 22);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(82, 21);
            this.radioButton1.TabIndex = 13;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Random";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(377, 22);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(159, 21);
            this.radioButton2.TabIndex = 14;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "On grain boundaries";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 50);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "Shape:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gbMC);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.gbShowEnergy);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(565, 801);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "MC";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gbMC
            // 
            this.gbMC.Controls.Add(this.label26);
            this.gbMC.Controls.Add(this.label18);
            this.gbMC.Controls.Add(this.btnMCSeed);
            this.gbMC.Controls.Add(this.btnMCIteration);
            this.gbMC.Controls.Add(this.cbMCDP);
            this.gbMC.Controls.Add(this.btnMCSimulation);
            this.gbMC.Controls.Add(this.numericUpDown4);
            this.gbMC.Controls.Add(this.numericUpDown5);
            this.gbMC.Controls.Add(this.textBox2);
            this.gbMC.Controls.Add(this.label17);
            this.gbMC.Location = new System.Drawing.Point(27, 22);
            this.gbMC.Name = "gbMC";
            this.gbMC.Size = new System.Drawing.Size(514, 202);
            this.gbMC.TabIndex = 18;
            this.gbMC.TabStop = false;
            this.gbMC.Text = "Monte Carlo";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(251, 154);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(124, 17);
            this.label26.TabIndex = 14;
            this.label26.Text = "Iterations to make:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(15, 27);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(95, 17);
            this.label18.TabIndex = 8;
            this.label18.Text = "Grains Count:";
            // 
            // btnMCSeed
            // 
            this.btnMCSeed.Location = new System.Drawing.Point(18, 53);
            this.btnMCSeed.Name = "btnMCSeed";
            this.btnMCSeed.Size = new System.Drawing.Size(225, 43);
            this.btnMCSeed.TabIndex = 0;
            this.btnMCSeed.Text = "Initialize Random Grains";
            this.btnMCSeed.UseVisualStyleBackColor = true;
            this.btnMCSeed.Click += new System.EventHandler(this.btnMCSeed_Click);
            // 
            // btnMCIteration
            // 
            this.btnMCIteration.Location = new System.Drawing.Point(18, 102);
            this.btnMCIteration.Name = "btnMCIteration";
            this.btnMCIteration.Size = new System.Drawing.Size(225, 36);
            this.btnMCIteration.TabIndex = 1;
            this.btnMCIteration.Text = "Single Iteration";
            this.btnMCIteration.UseVisualStyleBackColor = true;
            this.btnMCIteration.Click += new System.EventHandler(this.btnMCIteration_Click);
            // 
            // cbMCDP
            // 
            this.cbMCDP.AutoSize = true;
            this.cbMCDP.Checked = true;
            this.cbMCDP.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbMCDP.Location = new System.Drawing.Point(249, 65);
            this.cbMCDP.Name = "cbMCDP";
            this.cbMCDP.Size = new System.Drawing.Size(175, 21);
            this.cbMCDP.TabIndex = 13;
            this.cbMCDP.Text = "With Dual Phase / SRX";
            this.cbMCDP.UseVisualStyleBackColor = true;
            this.cbMCDP.CheckedChanged += new System.EventHandler(this.cbMCDP_CheckedChanged);
            // 
            // btnMCSimulation
            // 
            this.btnMCSimulation.Location = new System.Drawing.Point(18, 144);
            this.btnMCSimulation.Name = "btnMCSimulation";
            this.btnMCSimulation.Size = new System.Drawing.Size(225, 36);
            this.btnMCSimulation.TabIndex = 2;
            this.btnMCSimulation.Text = "Simulation";
            this.btnMCSimulation.UseVisualStyleBackColor = true;
            this.btnMCSimulation.Click += new System.EventHandler(this.btnMCSimulation_Click);
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.Location = new System.Drawing.Point(383, 152);
            this.numericUpDown4.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown4.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(96, 22);
            this.numericUpDown4.TabIndex = 3;
            this.numericUpDown4.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown4.ValueChanged += new System.EventHandler(this.numericUpDown4_ValueChanged_1);
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.Location = new System.Drawing.Point(147, 25);
            this.numericUpDown5.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numericUpDown5.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(96, 22);
            this.numericUpDown5.TabIndex = 7;
            this.numericUpDown5.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDown5.ValueChanged += new System.EventHandler(this.numericUpDown5_ValueChanged);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox2.Location = new System.Drawing.Point(383, 109);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(96, 22);
            this.textBox2.TabIndex = 4;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(251, 112);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(110, 17);
            this.label17.TabIndex = 6;
            this.label17.Text = "Current Iteration";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBox4);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this.textBox3);
            this.groupBox6.Controls.Add(this.label29);
            this.groupBox6.Controls.Add(this.btnRSX);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.nudRSXIterations);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this.cbRSXLocation);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.nudRSXIncrease);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.nudRSXNucleons);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.cbRsx);
            this.groupBox6.Location = new System.Drawing.Point(27, 447);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(514, 183);
            this.groupBox6.TabIndex = 17;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Recrystalization";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox4.Location = new System.Drawing.Point(406, 141);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(96, 22);
            this.textBox4.TabIndex = 33;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(274, 144);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(86, 17);
            this.label30.TabIndex = 34;
            this.label30.Text = "SRX Grains:";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox3.Location = new System.Drawing.Point(406, 113);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(96, 22);
            this.textBox3.TabIndex = 31;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(274, 116);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(110, 17);
            this.label29.TabIndex = 32;
            this.label29.Text = "Current Iteration";
            // 
            // btnRSX
            // 
            this.btnRSX.Enabled = false;
            this.btnRSX.Location = new System.Drawing.Point(277, 23);
            this.btnRSX.Name = "btnRSX";
            this.btnRSX.Size = new System.Drawing.Size(225, 36);
            this.btnRSX.TabIndex = 30;
            this.btnRSX.Text = "Simulation";
            this.btnRSX.UseVisualStyleBackColor = true;
            this.btnRSX.Click += new System.EventHandler(this.btnRSX_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(274, 79);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(124, 17);
            this.label27.TabIndex = 29;
            this.label27.Text = "Iterations to make:";
            // 
            // nudRSXIterations
            // 
            this.nudRSXIterations.Location = new System.Drawing.Point(406, 77);
            this.nudRSXIterations.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudRSXIterations.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudRSXIterations.Name = "nudRSXIterations";
            this.nudRSXIterations.Size = new System.Drawing.Size(96, 22);
            this.nudRSXIterations.TabIndex = 28;
            this.nudRSXIterations.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudRSXIterations.ValueChanged += new System.EventHandler(this.nudRSXIterations_ValueChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(15, 85);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(66, 17);
            this.label25.TabIndex = 27;
            this.label25.Text = "Location:";
            // 
            // cbRSXLocation
            // 
            this.cbRSXLocation.FormattingEnabled = true;
            this.cbRSXLocation.Location = new System.Drawing.Point(104, 81);
            this.cbRSXLocation.Name = "cbRSXLocation";
            this.cbRSXLocation.Size = new System.Drawing.Size(154, 24);
            this.cbRSXLocation.TabIndex = 26;
            this.cbRSXLocation.SelectedIndexChanged += new System.EventHandler(this.cbRSXLocation_SelectedIndexChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(15, 141);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(66, 17);
            this.label24.TabIndex = 25;
            this.label24.Text = "Increase:";
            // 
            // nudRSXIncrease
            // 
            this.nudRSXIncrease.Location = new System.Drawing.Point(129, 139);
            this.nudRSXIncrease.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudRSXIncrease.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudRSXIncrease.Name = "nudRSXIncrease";
            this.nudRSXIncrease.Size = new System.Drawing.Size(96, 22);
            this.nudRSXIncrease.TabIndex = 24;
            this.nudRSXIncrease.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(15, 113);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(71, 17);
            this.label23.TabIndex = 23;
            this.label23.Text = "Nucleons:";
            // 
            // nudRSXNucleons
            // 
            this.nudRSXNucleons.Location = new System.Drawing.Point(129, 111);
            this.nudRSXNucleons.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudRSXNucleons.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudRSXNucleons.Name = "nudRSXNucleons";
            this.nudRSXNucleons.Size = new System.Drawing.Size(96, 22);
            this.nudRSXNucleons.TabIndex = 22;
            this.nudRSXNucleons.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 55);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(47, 17);
            this.label19.TabIndex = 7;
            this.label19.Text = "Mode:";
            // 
            // cbRsx
            // 
            this.cbRsx.FormattingEnabled = true;
            this.cbRsx.Location = new System.Drawing.Point(104, 51);
            this.cbRsx.Name = "cbRsx";
            this.cbRsx.Size = new System.Drawing.Size(154, 24);
            this.cbRsx.TabIndex = 0;
            this.cbRsx.SelectedIndexChanged += new System.EventHandler(this.cbRsx_SelectedIndexChanged);
            // 
            // gbShowEnergy
            // 
            this.gbShowEnergy.Controls.Add(this.label28);
            this.gbShowEnergy.Controls.Add(this.nudJ);
            this.gbShowEnergy.Controls.Add(this.label22);
            this.gbShowEnergy.Controls.Add(this.nudEnergy2);
            this.gbShowEnergy.Controls.Add(this.label21);
            this.gbShowEnergy.Controls.Add(this.nudEnergy);
            this.gbShowEnergy.Controls.Add(this.label20);
            this.gbShowEnergy.Controls.Add(this.cbEnergyDistribution);
            this.gbShowEnergy.Controls.Add(this.cbShowEnergy);
            this.gbShowEnergy.Controls.Add(this.label16);
            this.gbShowEnergy.Controls.Add(this.btnDistributeEnergy);
            this.gbShowEnergy.Controls.Add(this.nudEnergyColors);
            this.gbShowEnergy.Location = new System.Drawing.Point(27, 249);
            this.gbShowEnergy.Name = "gbShowEnergy";
            this.gbShowEnergy.Size = new System.Drawing.Size(514, 173);
            this.gbShowEnergy.TabIndex = 16;
            this.gbShowEnergy.TabStop = false;
            this.gbShowEnergy.Text = "Energy";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(15, 72);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(133, 17);
            this.label28.TabIndex = 23;
            this.label28.Text = "Boundary multiplier:";
            // 
            // nudJ
            // 
            this.nudJ.DecimalPlaces = 2;
            this.nudJ.Location = new System.Drawing.Point(162, 70);
            this.nudJ.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudJ.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudJ.Name = "nudJ";
            this.nudJ.Size = new System.Drawing.Size(96, 22);
            this.nudJ.TabIndex = 22;
            this.nudJ.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudJ.ValueChanged += new System.EventHandler(this.nudJ_ValueChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(264, 69);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(104, 17);
            this.label22.TabIndex = 21;
            this.label22.Text = "Energy amount";
            // 
            // nudEnergy2
            // 
            this.nudEnergy2.Location = new System.Drawing.Point(406, 67);
            this.nudEnergy2.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudEnergy2.Name = "nudEnergy2";
            this.nudEnergy2.Size = new System.Drawing.Size(96, 22);
            this.nudEnergy2.TabIndex = 20;
            this.nudEnergy2.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudEnergy2.ValueChanged += new System.EventHandler(this.nudEnergy2_ValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(264, 24);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(104, 17);
            this.label21.TabIndex = 19;
            this.label21.Text = "Energy amount";
            // 
            // nudEnergy
            // 
            this.nudEnergy.Location = new System.Drawing.Point(406, 22);
            this.nudEnergy.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudEnergy.Name = "nudEnergy";
            this.nudEnergy.Size = new System.Drawing.Size(96, 22);
            this.nudEnergy.TabIndex = 18;
            this.nudEnergy.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudEnergy.ValueChanged += new System.EventHandler(this.nudEnergy_ValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(15, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 17);
            this.label20.TabIndex = 17;
            this.label20.Text = "Distribution:";
            // 
            // cbEnergyDistribution
            // 
            this.cbEnergyDistribution.FormattingEnabled = true;
            this.cbEnergyDistribution.Location = new System.Drawing.Point(129, 21);
            this.cbEnergyDistribution.Name = "cbEnergyDistribution";
            this.cbEnergyDistribution.Size = new System.Drawing.Size(129, 24);
            this.cbEnergyDistribution.TabIndex = 16;
            this.cbEnergyDistribution.SelectedIndexChanged += new System.EventHandler(this.cbEnergyDistribution_SelectedIndexChanged);
            // 
            // cbShowEnergy
            // 
            this.cbShowEnergy.AutoSize = true;
            this.cbShowEnergy.Enabled = false;
            this.cbShowEnergy.Location = new System.Drawing.Point(277, 108);
            this.cbShowEnergy.Name = "cbShowEnergy";
            this.cbShowEnergy.Size = new System.Drawing.Size(156, 21);
            this.cbShowEnergy.TabIndex = 12;
            this.cbShowEnergy.Text = "Toogle Energy View";
            this.cbShowEnergy.UseVisualStyleBackColor = true;
            this.cbShowEnergy.CheckedChanged += new System.EventHandler(this.cbShowEnergy_CheckedChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(274, 135);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 17);
            this.label16.TabIndex = 15;
            this.label16.Text = "Colors:";
            // 
            // btnDistributeEnergy
            // 
            this.btnDistributeEnergy.Location = new System.Drawing.Point(18, 116);
            this.btnDistributeEnergy.Name = "btnDistributeEnergy";
            this.btnDistributeEnergy.Size = new System.Drawing.Size(240, 36);
            this.btnDistributeEnergy.TabIndex = 11;
            this.btnDistributeEnergy.Text = "Distribute energy";
            this.btnDistributeEnergy.UseVisualStyleBackColor = true;
            this.btnDistributeEnergy.Click += new System.EventHandler(this.btnDistributeEnergy_Click);
            // 
            // nudEnergyColors
            // 
            this.nudEnergyColors.Location = new System.Drawing.Point(337, 135);
            this.nudEnergyColors.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudEnergyColors.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudEnergyColors.Name = "nudEnergyColors";
            this.nudEnergyColors.Size = new System.Drawing.Size(96, 22);
            this.nudEnergyColors.TabIndex = 14;
            this.nudEnergyColors.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudEnergyColors.ValueChanged += new System.EventHandler(this.nudEnergyColors_ValueChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1439, 873);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panelPicture);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1200, 900);
            this.Name = "Form1";
            this.Text = "Grain Growth";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pb)).EndInit();
            this.panelPicture.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBoundSize)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.gbMC.ResumeLayout(false);
            this.gbMC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRSXIterations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRSXIncrease)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRSXNucleons)).EndInit();
            this.gbShowEnergy.ResumeLayout(false);
            this.gbShowEnergy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEnergy2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEnergy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEnergyColors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb;
        private System.Windows.Forms.Panel panelPicture;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ToolStripMenuItem textFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.Label lblY;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbInclusionShape;
        private System.Windows.Forms.CheckBox cbInclusions;
        private System.Windows.Forms.CheckBox cbShowBoundaries;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TrackBar trackBar3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox tbGB;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown nudBoundSize;
        private System.Windows.Forms.CheckBox cbPeriodical;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnTransform;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbStructure;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnTransformStructure;
        private System.Windows.Forms.Button btnAddInclusions;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.Button btnMCSimulation;
        private System.Windows.Forms.Button btnMCIteration;
        private System.Windows.Forms.Button btnMCSeed;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.Button btnDistributeEnergy;
        private System.Windows.Forms.CheckBox cbShowEnergy;
        private System.Windows.Forms.CheckBox cbMCDP;
        private System.Windows.Forms.NumericUpDown nudEnergyColors;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox gbShowEnergy;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cbRsx;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown nudEnergy2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown nudEnergy;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cbEnergyDistribution;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown nudRSXNucleons;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox cbRSXLocation;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown nudRSXIncrease;
        private System.Windows.Forms.GroupBox gbMC;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btnRSX;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.NumericUpDown nudRSXIterations;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.NumericUpDown nudJ;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label29;
    }
}

