﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrainGrowth
{
    public partial class Form1 : Form
    {
        /*
         * TODO:
         * REFACTOR: 
         * {
         *  MOVE ALL CALCULATING/CELL CODE TO NEW CLASSES
         *  CHANGE CONTROLS NAMING
         *  GET RID OF REPEATABLE CODE (RULE1, 2, 3)
         * }
         * MULTITHREADING
        */
        private int x;
        private int y;
        private Cell[,] cells;
        private Cell[,] cellsPrev;
        private int Nucleons;
        private Bitmap bitmap;
        private int startPixels;
        private Random rand = new Random();
        private List<Cell> boundaryCells = new List<Cell>();
        private int inclusions;
        private int inclusionSize;
        private List<Cell> Inclusions = new List<Cell>();
        private Color black = Color.Black;
        private Color magenta = Color.Magenta;
        private Color red = Color.Red;
        private List<Cell> emptyCells = new List<Cell>();
        private const int SECOND_PHASE_ID = 99999999;

        private StructureType structureType;

        public Form1()
        {
            InitializeComponent();

            cbInclusionShape.Items.AddRange(Enum.GetNames(typeof(InclusionShape)));
            cbInclusionShape.SelectedIndex = 0;

            comboBox1.Items.AddRange(Enum.GetNames(typeof(NeighborHood)));
            comboBox1.SelectedIndex = 0;

            cbStructure.Items.AddRange(Enum.GetNames(typeof(StructureType)));
            cbStructure.SelectedIndex = 0;

            cbEnergyDistribution.Items.AddRange(Enum.GetNames(typeof(EnergyDistribution)));
            cbEnergyDistribution.SelectedIndex = 0;

            cbRsx.Items.AddRange(Enum.GetNames(typeof(SRXNucleation)));
            cbRsx.SelectedIndex = 0;

            cbRSXLocation.Items.AddRange(Enum.GetNames(typeof(SRXLocation)));
            cbRSXLocation.SelectedIndex = 0;

            radioButton1.Checked = true;
            cbInclusions.Checked = true;
        }

        List<int> importedIds = new List<int>();

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        importSuccess = true;
                        List<string> lines = reader.ReadToEnd().Split('\n').ToList();
                        
                        ProcessImportedDimensions(lines[0]);
                        ProcessImportedNucleons(lines);
                        ProcessImportedCells(lines);
                        if(!importSuccess)
                        {
                            MessageBox.Show("An error occured during import.\nPlease make sure your data is in correct format.");
                        }
                        else
                        {
                            trackBar1.Value = x;
                            trackBar2.Value = y;
                            Draw();
                        }

                        numericUpDown1.Value = Nucleons;
                        Progress(0);
                    }
                }
            }
        }

        private bool importSuccess = true;

        private void ProcessImportedDimensions(string line)
        {
            try
            {
                var parts = line.Split(' ');
                if (parts.Length == 2)
                {
                    x = int.Parse(parts[0]);
                    y = int.Parse(parts[1]);
                }
            }
            catch
            {
                importSuccess = false;
                return;
            }
        }

        private void ProcessImportedNucleons(List<string> lines)
        {
            try
            {
                Colors = new Dictionary<int, Color>();
                Colors.Add(0, Color.White);
                Nucleons = int.Parse(lines[1]);
                int r = 0;
                int g = 0;
                int b = 0;
                int id = 0;
                for(int i=0; i<Nucleons; i++)
                {
                    var line = lines[i+2];
                    var parts = line.Split(' ');
                    if (parts.Length == 4)
                    {
                        id = int.Parse(parts[0]);
                        r = int.Parse(parts[1]);
                        g = int.Parse(parts[2]);
                        b = int.Parse(parts[3].TrimEnd('\r'));
                        Colors.Add(id, Color.FromArgb(r, g, b));
                    }
                    else
                        throw new Exception();
                }
            }
            catch
            {
                importSuccess = false;
                return;
            }
        }

        private void ProcessImportedCells(List<string> lines)
        {
            try
            {
                var array = lines.ToArray();
                var newList = array.ToList();
                newList.RemoveRange(0, Nucleons + 1);
                MakeMatrices();
                for (int i = 1; i < newList.Count-1; i++)
                {
                    try
                    {
                        var line = newList[i].Split(' ');
                        int posx = int.Parse(line[0]);
                        int posy = int.Parse(line[1]);
                        int id = int.Parse(line[2].TrimEnd('\r'));
                        if (!importedIds.Contains(id))
                            importedIds.Add(id);
                        cellsPrev[posx, posy].Id = id;
                        cells[posx, posy].Id = id;
                    }
                    catch
                    {

                    }
                }
            }
            catch
            {
                importSuccess = false;
                return;
            }
        }

        private bool selectedGrow = false;

        private List<Cell> GetNeighbours(Cell cell, NeighborHood hood)
        {
            var list = new List<Cell>();

            int left = cell.X - 1;
            int right = cell.X + 1;
            int up = cell.Y + 1;
            int down = cell.Y - 1;

            if(hood == NeighborHood.VONNEUMANN)
            {
                list.Add(GetCellAt(left, cell.Y));
                list.Add(GetCellAt(right, cell.Y));
                list.Add(GetCellAt(cell.X, up));
                list.Add(GetCellAt(cell.X, down));
            }
            else if(hood == NeighborHood.MOORE)
            {
                list.Add(GetCellAt(left, cell.Y));
                list.Add(GetCellAt(right, cell.Y));
                list.Add(GetCellAt(cell.X, up));
                list.Add(GetCellAt(cell.X, down));
                list.Add(GetCellAt(left, up));
                list.Add(GetCellAt(right, up));
                list.Add(GetCellAt(right, down));
                list.Add(GetCellAt(left, down));
            }
            else if(hood == NeighborHood.DIFF)
            {
                list.Add(GetCellAt(left, up));
                list.Add(GetCellAt(right, up));
                list.Add(GetCellAt(right, down));
                list.Add(GetCellAt(left, down));
            }

            return list;
        }

        private Cell GetTorusCell(int posX, int posY)
        {
            if (posX < 0)
                posX = x-1;
            else if (posX >= x)
                posX = 0;

            if (posY < 0)
                posY = y-1;
            else if (posY >= y)
                posY = 0;

            return GetCellAt(posX, posY);
        }
        
        private Cell GetCellAt(int posX, int posY)
        {
            bool inValidX = (posX < 0 || posX >= x);
            bool inValidY = (posY < 0 || posY >= y);
            

            if((inValidX || inValidY) && periodical)
            {
                return GetTorusCell(posX, posY);
            }

            if(inValidX)
            {
                return null;
            }
            else if(inValidY)
            {
                return null;
            }
            else
            {
                return cellsPrev[posX, posY];
            }
        }
        
        private bool FirstRule(ref Cell cell)
        {
            var ids = GetNeighbours(cell, NeighborHood.MOORE).Where(c => c != null && c.Id != SECOND_PHASE_ID).Select(c => c.Id).ToList();
            Dictionary<int, int> values = new Dictionary<int, int>();
            int max = -1;
            foreach (int i in ids)
            {
                var c = ids.Where(o => o > 0 && o == i).Count();
                if (c > 4 && !values.ContainsKey(i))
                    values.Add(i, c);
            }
            if (values.Count == 0)
                return false;
            else
            {
                var temp = values.OrderBy(v => v.Value).ToList();
                var maxIds = values.Where(v => v.Value == temp.Last().Value).Select(v => v.Key);
                max = maxIds.ElementAt(rand.Next(maxIds.Count()));
                cell.Id = max;
                return true;
            }
        }

        private bool SecondRule(ref Cell cell)
        {
            var ids = GetNeighbours(cell, NeighborHood.VONNEUMANN).Where(c => c != null && c.Id != SECOND_PHASE_ID).Select(c => c.Id).ToList();
            Dictionary<int, int> values = new Dictionary<int, int>();
            int max = -1;
            foreach (int i in ids)
            {
                var c = ids.Where(o => o > 0 && o == i).Count();
                if (c > 2 && !values.ContainsKey(i))
                    values.Add(i, c);
            }
            if (values.Count == 0)
                return false;
            else
            {
                var temp = values.OrderBy(v => v.Value).ToList();
                var maxIds = values.Where(v => v.Value == temp.Last().Value).Select(v => v.Key);
                max = maxIds.ElementAt(rand.Next(maxIds.Count()));
                cell.Id = max;
                return true;
            }
        }

        private bool ThirdRule(ref Cell cell)
        {
            var ids = GetNeighbours(cell, NeighborHood.DIFF).Where(c => c != null && c.Id != SECOND_PHASE_ID).Select(c => c.Id).ToList();
            Dictionary<int, int> values = new Dictionary<int, int>();
            int max = -1;
            foreach (int i in ids)
            {
                var c = ids.Where(o => o > 0 && o == i).Count();
                if (c > 2 && !values.ContainsKey(i))
                    values.Add(i, c);
            }
            if (values.Count == 0)
                return false;
            else
            {
                var temp = values.OrderBy(v => v.Value).ToList();
                var maxIds = values.Where(v => v.Value == temp.Last().Value).Select(v => v.Key);
                max = maxIds.ElementAt(rand.Next(maxIds.Count()));
                cell.Id = max;
                return true;
            }
        }

        private NeighborHood selectedHood = NeighborHood.VONNEUMANN;

        private int GetNewIdForCell(Cell cell)
        {
            List<int> ids = GetNeighbours(cell, selectedHood).Where(c => c != null && c.Id != SECOND_PHASE_ID).Select(c => c.Id).ToList();
            Dictionary<int, int> values = new Dictionary<int, int>();
            int max = -1;
            foreach (int i in ids)
            {
                var c = ids.Where(o => o > 0 && o == i).Count();
                if (c > 0 && !values.ContainsKey(i))
                    values.Add(i, c);
            }

            if (values.Count == 0)
                return 0;

            var temp = values.OrderBy(v => v.Value).ToList();
            var maxIds = values.Where(v => v.Value == temp.Last().Value).Select(v => v.Key);

            max = maxIds.ElementAt(rand.Next(maxIds.Count()));
            return max;
        }
        
        private void ProcessCell(Cell cell)
        {
            if(cell.Id > 0 || cell.Id == -1 || cell.Id == SECOND_PHASE_ID || (!selectedGrow && selectedIdsBefore.Contains(cell.Id)))
            {
                return;
            }

            if(useModifiedRules)
            {
                if (!FirstRule(ref cell))
                {
                    if (!SecondRule(ref cell))
                    {
                        if (!ThirdRule(ref cell))
                        {
                            var newId = GetNewIdForCell(cell);

                            var propability = (decimal)(rand.Next(100) / 100m);

                            if (newId > 0 && propability <= percent)
                            {
                                cells[cell.X, cell.Y].Id = newId;
                                return;
                            }
                        }
                    }
                }
                cells[cell.X, cell.Y].Id = cell.Id;
            }
            else
            {
                var newId = GetNewIdForCell(cell);

                if (newId > 0)
                {
                    cells[cell.X, cell.Y].Id = newId;
                    return;
                }
            }
        }
        
        private void FindBoundaries()
        {
            boundaryCells = new List<Cell>();
            var neighbours = 0;
            cellsPrev.Cast<Cell>().ToList().ForEach(c => c.Boundary = false);
            cells.Cast<Cell>().ToList().ForEach(c => c.Boundary = false);
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    neighbours = GetNeighbours(cells[i, j], NeighborHood.MOORE)
                        .Where(n => n != null && n?.Id != 0 && n?.Id != SECOND_PHASE_ID && n?.Id != -1)
                        .Select(c => c.Id)
                        .Distinct()
                        .Count();

                    if (neighbours > 1)
                    {
                        cells[i, j].Boundary = true;
                        cellsPrev[i, j].Boundary = true;
                        boundaryCells.Add(cells[i, j]);
                    }
                }
            }
            NewTimeStep(true);
            for (int k=1; k<boundarySize; k++)
            {
                for (int i = 0; i < x; i++)
                {
                    for (int j = 0; j < y; j++)
                    {
                        if (cellsPrev[i,j].Boundary || cells[i, j].Boundary || boundaryCells.Contains(cells[i, j]))
                            continue;

                        var temp = GetNeighbours(cells[i, j], NeighborHood.MOORE).Where(n => n != null && n.Boundary).ToList();
                        if(temp.Any())
                        {
                            cells[i, j].Boundary = true;
                            //cellsPrev[i, j].Boundary = true;
                            boundaryCells.Add(cells[i, j]);
                        }
                    }
                }
                NewTimeStep(true);
            }

        }

        private bool printing = false;
        private int empty = -1;

        private bool isCalculating = false;
        private void button1_Click(object sender, EventArgs e)
        {
            if(!isSeeded)
            {
                button2.BackColor = Color.IndianRed;
                return;
            }
            cbShowBoundaries.Checked = false;
            label4.Visible = false;
            button1.Enabled = false;
            Nucleons = (int)numericUpDown1.Value;
            x = trackBar1.Value;
            y = trackBar2.Value;
            startPixels = x * y;
            if (isSeeded)
            {
                Thread thread = new Thread(new ThreadStart(Start));
                thread.Start();
            }
        }

        private int existingNucleons = 0;

        private void AddColorForId(int id)
        {
            if (Colors.ContainsKey(id)) return;

            Color color = GetUniqueColor();
            Colors.Add(id, color);
        }

        private Color GetUniqueColor()
        {
            Random a = new Random();
            Color newColor;
            int r = a.Next(colorMinValue, colorMaxValue);
            int g = a.Next(colorMinValue, colorMaxValue);
            int b = a.Next(colorMinValue, colorMaxValue);
            newColor = Color.FromArgb(r, g, b);
            while (Colors.Any(c => c.Value == newColor))
            {
                r = a.Next(colorMinValue, colorMaxValue);
                g = a.Next(colorMinValue, colorMaxValue);
                b = a.Next(colorMinValue, colorMaxValue);
                newColor = Color.FromArgb(r, g, b);
            }
            return newColor;
        }

        private Color GetUniqueColorSrx()
        {
            Random a = new Random();
            Color newColor;
            int r = a.Next(colorMinValue, colorMaxValue+1);
            newColor = Color.FromArgb(r, 0, 0);
            while (_srxColors.Any(c => c.Value == newColor))
            {
                r = a.Next(colorMinValue, colorMaxValue);
                newColor = Color.FromArgb(r, 0, 0);
            }
            return newColor;
        }

        private void Seed()
        {
            trackBar1_Scroll(null, null);
            trackBar2_Scroll(null, null);
            x = trackBar1.Value;
            y = trackBar2.Value;

            if (mcGrains > 0 || mcIterations > 0)
                isSeeded = true;

            if (!isSeeded || cells?.Length != x * y)
                MakeMatrices(!transformBoundaries);
            if (Colors == null)
                GetColors();
            
            Draw();
            var map = pb.Image as Bitmap;
            Color oldColor;
            bool isOnWhite = false;
            Color white = Color.FromArgb(255, 255, 255, 255);
            Random a = new Random();
            Cell emptyCell;
            int _x = 0;
            int _y = 0;
            List<Point> points = new List<Point>();
            for (int i = existingNucleons; i < existingNucleons + Nucleons; i++)
            {
                if (!Colors.ContainsKey(i + 1))
                    AddColorForId(i + 1);

                isOnWhite = false;
                int randInd = a.Next(emptyCells.Count-1);
                emptyCell = emptyCells[randInd];
                _x = emptyCell.X;
                _y = emptyCell.Y;

                while ((points.Any(p => p.X == _x && p.Y == _y) && boundaryCells.Any(b => b.X == _x && b.Y == _y)) || !isOnWhite)
                {
                    oldColor = map?.GetPixel(_x, _y) ?? white;
                    isOnWhite = oldColor.Equals(white);
                }
                emptyCells.Remove(emptyCell);
                points.Add(new Point(_x, _y));
                
                var cell = new Cell { Id = i + 1, X = _x, Y = _y };
                cellsPrev[_x, _y] = cell;
                cells[_x, _y] = cell;
            }
            existingNucleons += Nucleons;
            if(transformBoundaries)
            {
                AddBoundariesAsInclusions();
                transformBoundaries = false;
            }
            isSeeded = true;
            Draw(true);
        }

        private void AddBoundariesAsInclusions()
        {
            boundaryCells.ForEach(c => cells[c.X, c.Y].Id = -1);
            boundaryCells.Clear();
        }

        private void MakeMatrices(bool clearBoundaries = true)
        {
            if (!clearBoundaries)
            {
                NewTimeStep(true);
            }
            else
            {
                cells = new Cell[x, y];
                cellsPrev = new Cell[x, y];
            }
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if (((cells?[i, j]?.Boundary ?? false) || (cellsPrev?[i, j]?.Boundary ?? false)))
                    {
                        continue;
                    }
                    cells[i, j] = new Cell { Id = 0, X = i, Y = j };
                    cellsPrev[i, j] = new Cell { Id = 0, X = i, Y = j };
                }
            }
        }

        bool isDrawing = false;

        private void Start()
        {
            Inclusions = new List<Cell>();
            if (isCalculating) return;
            isCalculating = true;
            Init();
            NewTimeStep();
            Simulate();
            isDrawing = false;
            isCalculating = false;
            //AddInclusions(inclusionMode);
            Draw();
        }

        private void Simulate()
        {
            empty = startPixels;
            int emptyPrev = -1;
            while (empty > 0 && empty != emptyPrev)
            {
                emptyPrev = empty;
                empty = GetEmptyCells();
                Progress(100 - empty * 100 / startPixels);
                Iterate();
            }
            Progress(100);
        }
        
        private void AddInclusions(InclusionMode mode)
        {
            if(!(mode == InclusionMode.Random && empty > 0 || mode == InclusionMode.OnGrainBoundaries))
            {
                return;
            }
            if(mode == InclusionMode.OnGrainBoundaries)
            {
                FindBoundaries();
            }
            for(int i=0; i<(int)numericUpDown2.Value; i++)
            {
                var location = GenerateInclusionLocation();
                AddInclusion(location.X, location.Y);
            }
        }

        private Point GenerateInclusionLocation()
        {
            Point point = new Point(0, 0);

            switch (inclusionMode)
            {
                case InclusionMode.None:
                    break;
                case InclusionMode.OnGrainBoundaries:
                    var cell = GetRandomBoundaryCell();
                    point.X = cell.X;
                    point.Y = cell.Y;
                    break;
                case InclusionMode.Random:
                    point.X = rand.Next(x);
                    point.Y = rand.Next(y);
                    break;
            }

            return point;
        }

        private Cell GetRandomBoundaryCell(bool onlyNonRecrystalized = false)
        {
            int index = rand.Next(boundaryCells.Count);
            Cell cell = boundaryCells[index];

            if(onlyNonRecrystalized)
            {
                if (cell.Recrystalized)
                    return GetRandomBoundaryCell(true);
            }

            return cell;
        }

        private int inclusionHalfSize = 0;

        private void AddInclusion(int posx, int posy)
        {
            if (inclusionMode == InclusionMode.None)
                return;
            
            List<Point> pointsToPopulate = new List<Point>();
            var center = new Point(posx, posy);
            switch (inclusionShape)
            {
                case InclusionShape.CIRCULAR:
                    pointsToPopulate.AddRange(MakeRect(posx, posy));
                    pointsToPopulate = pointsToPopulate.Where(p => IsInRadius(p, center, inclusionSize/2)).Distinct().ToList();
                    break;

                case InclusionShape.SQUARE:
                    pointsToPopulate.AddRange(MakeRect(posx, posy));
                    break;
            }

            foreach(var p in pointsToPopulate)
            {
                cells[p.X, p.Y].Id = -1;
                cellsPrev[p.X, p.Y].Id = -1;
                Inclusions.Add(cells[p.X, p.Y]);
            }

        }

        private bool IsInRadius(Point p, Point center, int r)
        {
            Point temp = new Point(center.X - p.X, center.Y - p.Y);
            double distance = VectorLen(temp);
            return (distance <= r);
        }

        private double VectorLen(Point p)
        {
            return Math.Sqrt((p.X * p.X) + (p.Y * p.Y));
        }

        private List<Point> MakeRect(int posx, int posy)
        {
            var points = new List<Point>();
            int newX = 0;
            int newY = 0;
            var center = new Point(posx, posy);
            for (int i = -inclusionHalfSize; i < inclusionHalfSize; i++)
            {
                for (int j = -inclusionHalfSize; j < inclusionHalfSize; j++)
                {
                    newX = center.X - i;
                    newY = center.Y + j;
                    if (newX >= 0 && newX < x && newY >= 0 && newY < y)
                    {
                        points.Add(new Point(newX, newY));
                    }
                }
            }
            return points;
        }

        private void Init()
        {
            GetEmptyCells();
            //AddInclusions(inclusionMode);
            //Draw();
        }

        private void Print()
        {
            if (!printing) return;

            string text = "";
            for(int i=0; i<x; i++)
            {
                for(int j=0; j<y;j++)
                {
                    text += cells[i, j].Id + "  ";
                }
                text += "\n";
            }
            MessageBox.Show(text);
        }

        delegate void DrawDelegate(bool status);

        private void Draw(bool withBoundaries = false)
        {
            if (InvokeRequired)
                Invoke(new DrawDelegate(Draw), cbShowBoundaries.Checked);
            else
            {
                if (isDrawing) return;
                isDrawing = true;
                emptyCells.Clear();
                var ccc = cellsPrev.Cast<Cell>().ToList().Where(c => c.Id == SECOND_PHASE_ID).ToList();

                withBoundaries = cbShowBoundaries.Checked;
                if(tabControl1.SelectedTab == tabPage1)
                    GetColors();
                cbShowBoundaries.Enabled = true;
                tbGB.Text = "";
                int id = 0;
              
                bitmap = new Bitmap(x, y);
                
                Cell cell;
                if (withBoundaries)
                {
                    for (int i = 0; i < x; i++)
                    {
                        for (int j = 0; j < y; j++)
                        {
                            cell = cells[i, j];
                            if (cell.Id == -1 || (Inclusions.Any(c => c.X == i && c.Y == j)))
                            {
                                bitmap.SetPixel(i, j, black);
                            }
                            else if(cell.Recrystalized || cell.Id < -1)
                            {
                                bitmap.SetPixel(i, j, _srxColors[cell.Id]);
                            }
                            else if(cell.Id == SECOND_PHASE_ID)
                            {
                                bitmap.SetPixel(i, j, magenta);
                            }
                            else
                            {
                                if (!cell.Boundary)
                                {
                                    id = cell.Id;
                                    if (id == 0)
                                        emptyCells.Add(cell);
                                    bitmap.SetPixel(i, j, Colors[id]);
                                }
                                else
                                    bitmap.SetPixel(i, j, black);
                            }
                        }
                        int boundaries = boundaryCells.Count * 100;
                        var percent = boundaries / startPixels;
                        var text = percent + " %";
                        tbGB.Text = text;
                    }
                }
                else
                {
                    for (int i = 0; i < x; i++)
                    {
                        for (int j = 0; j < y; j++)
                        {
                            cell = cells[i, j];
                            if (cell.Id == -1 || (Inclusions.Any(c => c.X == i && c.Y == j)))
                            {
                                bitmap.SetPixel(i, j, black);
                            }
                            else if (cell.Recrystalized || cell.Id < -1)
                            {
                                bitmap.SetPixel(i, j, _srxColors[cell.Id]);
                            }
                            else if(cell.Id == SECOND_PHASE_ID)
                            {
                                bitmap.SetPixel(i, j, magenta);
                            }
                            else
                            {
                                id = cell.Id;
                                if (id == 0)
                                    emptyCells.Add(cell);
                                bitmap.SetPixel(i, j, Colors[id]);
                            }
                        }
                    }
                }
                trackBar1_Scroll(null, null);
                trackBar2_Scroll(null, null);
                pb.Image = bitmap;
                button1.Enabled = true;
                isDrawing = false;
            }
        }

        Dictionary<int, Color> Colors;

        private const int colorMinValue = 1;
        private const int colorMaxValue = 254;

        private void GetColors()
        {
            if (Colors == null || (Colors.Count() != existingNucleons+1 && Colors.Count != mcGrains+1))
                Colors = new Dictionary<int, Color>();
            else
                return;

            Colors.Add(0, Color.White);
            for (int i=1; i<=Nucleons; i++)
            {                
                Colors.Add(i, GetUniqueColor());
            }
        }

        private int GetEmptyCells()
        {
            if (cellsPrev == null)
                MakeMatrices();

            var _cells = cellsPrev.Cast<Cell>();

            int count = _cells.Where(c => c.Id == 0).Count();
            empty = count;
            return count;
        }

        private void Iterate()
        {
            foreach (var cell in cells)
            {
                if(cell.Id != -1)
                    ProcessCell(cell);
            }
            NewTimeStep();
        }

        private void NewTimeStep(bool withBoundaries = false)
        {
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if (withBoundaries)
                        cellsPrev[i, j].Boundary = cells[i, j].Boundary;
                    else
                        cellsPrev[i, j].Id = cells[i, j].Id;
                    cellsPrev[i, j].Energy = cells[i, j].Energy;
                    cellsPrev[i, j].Recrystalized = cells[i, j].Recrystalized;
                    if(cells[i, j].Recrystalized)
                        cellsPrev[i, j].Id = cells[i, j].Id;
                }
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            int value = trackBar1.Value;
            lblX.Text = value.ToString();
            pb.Size = new Size(value, pb.Size.Height);
            CenterPictureBox();
            UpdateStartPixels();
        }

        private void UpdateStartPixels()
        {
            startPixels = trackBar2.Value * trackBar1.Value;
            numericUpDown1.Maximum = Math.Min(startPixels - 1, SECOND_PHASE_ID - 1);
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            int value = trackBar2.Value;
            lblY.Text = value.ToString();
            pb.Size = new Size(pb.Size.Width, value);
            CenterPictureBox();
            UpdateStartPixels();
        }
        
        private void CenterPictureBox()
        {
            trackBar1.Maximum = panelPicture.Width * 99 / 100;
            trackBar2.Maximum = panelPicture.Height * 99 / 100;
            int posx = panelPicture.Width / 2 - pb.Width / 2;
            int posy = panelPicture.Height / 2 - pb.Height / 2;
            pb.Location = new Point(posx, posy);
        }

        private delegate void ProgressDelegate(int percent);

        private bool isProgress = false;

        private void Progress(int percent)
        {
            if(InvokeRequired)
            {
                Invoke(new ProgressDelegate(Progress), percent);
            }
            else
            {
                if (isProgress)
                    return;
                isProgress = true;
                progressBar1.Value = percent;
                if(percent >= 100)
                {
                    label4.Visible = true;
                }
                else if(label4.Visible)
                {
                    label4.Visible = false;
                }
                isProgress = false;
            }
        }

        private void textFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filePath = string.Empty;

            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.InitialDirectory = "c:\\";
                saveFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                saveFileDialog.FilterIndex = 1;
                saveFileDialog.RestoreDirectory = true;

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = saveFileDialog.FileName;
                    var writer = File.AppendText(filePath);
                    writer.WriteLine($"{x} {y}");
                    writer.WriteLine(Nucleons);
                    string line = "";
                    for(int i=0; i<Nucleons; i++)
                    {
                        writer.WriteLine($"{i+1} {Colors[i+1].R} {Colors[i+1].G} {Colors[i+1].B}");
                    }
                    Cell temp;
                    List<int> ids = new List<int>();
                    for(int i=0; i<x; i++)
                    {
                        for(int j=0; j<y; j++)
                        {
                            temp = cells[i, j];
                            if (!ids.Contains(temp.Id))
                                ids.Add(temp.Id);
                            line = $"{temp.X} {temp.Y} {temp.Id}";
                            writer.WriteLine(line);
                        }
                    }
                    writer.Close();
                }
            }

        }

        private void imageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cbShowBoundaries.Checked = false;
            //cbShowBoundaries_CheckedChanged(null, null);
            var filePath = string.Empty;

            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.InitialDirectory = "c:\\";
                saveFileDialog.Filter = "bmp files (*.bmp)|*.bmp|All files (*.*)|*.*";
                saveFileDialog.FilterIndex = 1;
                saveFileDialog.RestoreDirectory = true;

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath = saveFileDialog.FileName;
                    pb.Image.Save(filePath);
                }
            }
        }

        private void cbInclusionShape_SelectedIndexChanged(object sender, EventArgs e)
        {
            inclusionShape = (InclusionShape)Enum.Parse(typeof(InclusionShape), cbInclusionShape.SelectedItem.ToString());
        }

        private InclusionShape inclusionShape;
        private InclusionMode inclusionMode;

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if(!isCalculating && radioButton1.Checked && !radioButton2.Checked)
            {
                inclusionMode = InclusionMode.Random;
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (!isCalculating && radioButton2.Checked && !radioButton1.Checked)
            {
                inclusionMode = InclusionMode.OnGrainBoundaries;
            }
        }

        private bool enableInclusions = true;
        private void cbInclusions_CheckedChanged(object sender, EventArgs e)
        {
            if (isCalculating) return;
            
            radioButton1.Enabled = radioButton2.Enabled = cbInclusionShape.Enabled = numericUpDown2.Enabled = numericUpDown3.Enabled = enableInclusions;
            if(!enableInclusions)
            {
                inclusionMode = InclusionMode.None;
            }
        }

        private void cbShowBoundaries_CheckedChanged(object sender, EventArgs e)
        {
            if (isCalculating) return;
            if(cbShowBoundaries.Checked)
                FindBoundaries();
            Draw(cbShowBoundaries.Checked);
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            if (isCalculating) return;

            inclusions = (int)numericUpDown2.Value;
            if (inclusions > startPixels)
                inclusions = startPixels - 1;
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            if (isCalculating) return;

            inclusionSize = (int)numericUpDown3.Value;
            if (inclusionSize > trackBar1.Value)
                inclusionSize = trackBar1.Value - 1;
            if (inclusionSize > trackBar2.Value)
                inclusionSize = trackBar2.Value - 1;
            inclusionHalfSize = inclusionSize / 2;
        }

        decimal percent = 0;
        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            percent = (decimal)(trackBar3.Value/100m);
            label10.Text = percent.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            trackBar1.Maximum = (int)(panelPicture.Width * 0.99);
            trackBar2.Maximum = (int)(panelPicture.Height * 0.99);

            trackBar1.Value = trackBar1.Minimum;
            trackBar1_Scroll(null, null);
            trackBar2.Value = trackBar2.Minimum;
            trackBar2_Scroll(null, null);
            trackBar3.Value = trackBar3.Maximum;
            x = trackBar1.Value;
            y = trackBar2.Value;

            Form1_SizeChanged(null, null);
        }
        private bool isSeeded = false;

        private bool useModifiedRules = false;

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            useModifiedRules = checkBox1.Checked;
            if (!useModifiedRules)
            {
                trackBar3.Value = 100;
                trackBar3_Scroll(null, null);
                trackBar3.Enabled = false;
            }
            label9.Enabled = useModifiedRules;
            trackBar3.Enabled = useModifiedRules;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedHood = (NeighborHood)Enum.Parse(typeof(NeighborHood), comboBox1.SelectedItem.ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.BackColor = Color.White;

            Seed();
            button1.Enabled = true;
            btnAddInclusions.Enabled = true;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            Nucleons = (int)numericUpDown1.Value;
        }

        private void panelPicture_SizeChanged(object sender, EventArgs e)
        {
            CenterPictureBox();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            panelPicture.Width = this.Width - panel2.Width;
        }

        private int boundarySize = 1;
        private void nudBoundSize_ValueChanged(object sender, EventArgs e)
        {
            boundarySize = (int)nudBoundSize.Value;
            cbShowBoundaries_CheckedChanged(null, null);
        }

        private bool periodical = false;
        private void cbPeriodical_CheckedChanged(object sender, EventArgs e)
        {
            periodical = cbPeriodical.Checked;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            existingNucleons = 0;
            MakeMatrices();
            boundaryCells = new List<Cell>();
            Inclusions = new List<Cell>();
            Draw();
        }

        private void TransformBoundaries()
        {
            for(int i=0; i<x; i++)
            {
                for(int j=0; j<y; j++)
                {
                    if (cells[i, j].Boundary)
                        cells[i, j].Id = -1;
                    else if(cells[i,j].Id > -1)
                        cells[i, j].Id = 0;
                }
            }
            //NewTimeStep();
        }

        private bool transformBoundaries = false;

        private void btnTransform_Click(object sender, EventArgs e)
        {
            existingNucleons = 0;
            Colors = null;
            transformBoundaries = true;
            TransformBoundaries();
            Draw();
        }

        private List<int> selectedIds = new List<int>();

        private void pb_MouseClick(object sender, MouseEventArgs e)
        {
            int id = cells[e.X, e.Y].Id;
            
            Select(id);
        }
        
        private void Select(int id)
        {
            if (id < 1)
                return;

            bool isSelected = selectedIds.Contains(id);
            ColorizeGrain(id, isSelected);

            if (!isSelected)
            {
                selectedIds.Add(id);
            }
            else
            {
                selectedIds.Remove(id);
            }
            cbStructure.Enabled = selectedIds.Any();
            textBox1.Text = selectedIds.Count.ToString();
        }

        private void ColorizeGrain(int id, bool reverse = false)
        {
            if (id <= 0 || Colors == null || Colors?.Count < 1 || id == SECOND_PHASE_ID) return;
            var color = reverse ? Colors[id] : magenta;
            var selectedCells = cells.Cast<Cell>().Where(c => c.Id == id).ToList();
            var map = pb.Image as Bitmap;
            selectedCells.ForEach(c => map.SetPixel(c.X, c.Y, color));
            pb.Image = map;
        }

        private void cbStructure_SelectedIndexChanged(object sender, EventArgs e)
        {
            structureType = (StructureType)Enum.Parse(typeof(StructureType), cbStructure.SelectedItem.ToString());
        }

        private void btnAddInclusions_Click(object sender, EventArgs e)
        {
            GetEmptyCells();
            AddInclusions(inclusionMode);
            Draw(true);
        }

        private void btnTransformStructure_Click(object sender, EventArgs e)
        {
            switch (structureType)
            {
                case StructureType.Substructure:
                    {
                        TransformToSubstructure();
                        break;
                    }
                case StructureType.DualPhase:
                    {
                        TransformToDualPhase();
                        break;
                    }
            }
            Draw(cbShowBoundaries.Checked);
            DeselectAll();
        }

        private void DeselectAll()
        {
            selectedIdsBefore.Clear();
            selectedIdsBefore.AddRange(selectedIds);
            for (int i=selectedIds.Count-1; i>=0; i--)
            {
                Select(selectedIds[i]);
            }
        }

        List<int> selectedIdsBefore = new List<int>();

        private void TransformToSubstructure()
        {
            Cell cell;
            substructureIds.Clear();
            substructureIds.AddRange(selectedIds);
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    cell = cells[i, j];

                    if(!(cell.Id < 1 || selectedIds.Contains(cell.Id)))
                    {
                        cells[i, j].Id = 0;
                    }
                }
            }
            
        }

        private void TransformToDualPhase()
        {
            Cell cell;
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    cell = cells[i, j];

                    if(selectedIds.Contains(cell.Id))
                    {
                        cells[i, j].Id = SECOND_PHASE_ID;
                    }
                    else if (cell.Id > 0)
                    {
                        cells[i, j].Id = 0;
                    }
                }
            }
            mcSeedDualPhase = true;
        }
        
        List<int> substructureIds = new List<int>();

        private int mcIterationsMax = 0;
        private int mcIterations = 0;
        private int mcGrains;

        private void btnMCSeed_Click(object sender, EventArgs e)
        {
            MC();
            SetMCEnable(true);
        }

        private void MC()
        {
            mcIterations = 0;
            textBox2.Text = "0";
            trackBar1_Scroll(null, null);
            trackBar2_Scroll(null, null);
            mcGrains = (int)numericUpDown5.Value;
            x = trackBar1.Value;
            y = trackBar2.Value;
            Task task = new Task(MCSeedRandom);
            task.Start();
            Task.WaitAll(task);
            if (!mcSeedDualPhase)
            {
                SRXiterations = 0;
                _singleSRXDone = false;
                textBox3.Text = SRXiterations.ToString();
                textBox4.Text = "0";
            }
            Draw();
        }

        private Random r = new Random();

        private void MCSeedRandom()
        {
            _recrystalizedIds = new List<int>();
            _srxColors = new Dictionary<int, Color>();

            if(!mcSeedDualPhase)
                MakeMatrices();

            GetColorsMC();
            int newId = 0;
            for(int i=0; i<x; i++)
            {
                for(int j=0; j<y; j++)
                {
                    if(cells[i, j].Id == 0)
                    {
                        newId = r.Next(mcGrains) + 1;
                        cellsPrev[i, j].Id = newId;
                        cells[i, j].Id = newId;
                    }
                }
            }
            mcCells = cells.Cast<Cell>().ToList();
            mcCellsPrev = cellsPrev.Cast<Cell>().ToList();
            return;
        }

        List<Cell> mcCells = new List<Cell>();
        List<Cell> mcCellsPrev = new List<Cell>();

        private void GetColorsMC()
        {
            Colors = new Dictionary<int, Color>();
            Colors.Add(0, Color.White);
            for(int i=0; i<mcGrains; i++)
            {
                Colors.Add(i+1, GetUniqueColor());
            }
        }

        Thread mcThread;

        private void MakeMCIterations()
        {
            if(MCIterationCount > 1)
                SetMCEnable(false);
            //if(mcThread != null && mcThread.IsAlive)
            //mcThread.Join();
            mcThread = new Thread(new ThreadStart(MCIterations));
            mcThread.Start();
            
        }

        private void btnMCIteration_Click(object sender, EventArgs e)
        {
            MCIterationCount = 1;
            MakeMCIterations();
        }

        private int MCIterationCount = 0;
        private void MCIterations()
        {
            for (int i = 0; i < MCIterationCount; i++)
            {
                int index = 0;
                int count = mcCells.Count;
                while (count > 0)
                {
                    index = r.Next(mcCells.Count);
                    ProcessMC(mcCells[index]);
                    mcCells.RemoveAt(index);
                    count--;
                }
                NewTimeStep();
                mcCells = cells.Cast<Cell>().ToList();
                mcCellsPrev = cellsPrev.Cast<Cell>().ToList();
                Draw();
                UpdateIterations(++mcIterations);
            }
            SetMCEnable(true);
        }

        private void UpdateIterations(int count)
        {
            if (InvokeRequired)
                Invoke(new ProgressDelegate(UpdateIterations), count);
            else
            {
                textBox2.Text = count.ToString();
            }
        }

        private void ProcessMC(Cell cell)
        {
            if (cell.Id == SECOND_PHASE_ID)
                return;

            var n = GetNeighbours(cell, NeighborHood.MOORE).Where(c => c != null).Select(c => c.Id).ToList();
            if (!n.Any(i => i != cell.Id))
            {
                cells[cell.X, cell.Y].Energy = 8;
                return;
            }

            double currentEnergy = GetEnergy(cell.Id, n);
            var distinct = n.Distinct().ToList();
            distinct.Remove(cell.Id);
            int newId = r.Next(mcGrains)+1;//distinct[r.Next(distinct.Count)];
            double newEnergy = GetEnergy(newId, n);

            double dE = newEnergy - currentEnergy;

            if(dE <= 0)
            {
                cells[cell.X, cell.Y].Id = newId;
                cells[cell.X, cell.Y].Energy = newEnergy;
            }
            else
            {
                cells[cell.X, cell.Y].Energy = currentEnergy;
            }
        }

        private double J = 1;
        private double energyToDistribute = 0;

        private double GetEnergy(int id, List<int> neighbors)
        {
            return neighbors.Where(i => i != id).Count() * J;
        }

        private void btnMCSimulation_Click(object sender, EventArgs e)
        {
            numericUpDown4_ValueChanged_1(null, null);
            MCIterationCount = mcIterationsMax;
            
            MakeMCIterations();
        }

        private void SetMCEnable(bool state)
        {
            if(InvokeRequired)
            {
                Invoke(new DrawDelegate(SetMCEnable), state);
            }
            else
            {
                btnMCIteration.Enabled = btnMCSimulation.Enabled = numericUpDown4.Enabled = btnDistributeEnergy.Enabled = state;
            }
        }

        private void numericUpDown4_ValueChanged_1(object sender, EventArgs e)
        {
            mcIterationsMax = (int)numericUpDown4.Value;
        }

        private void numericUpDown5_ValueChanged(object sender, EventArgs e)
        {
            mcGrains = (int)numericUpDown5.Value;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedIndex)
            {
                case 0:

                    break;

                case 1:
                    trackBar1_Scroll(null, null);
                    trackBar2_Scroll(null, null);
                    x = trackBar1.Value;
                    y = trackBar2.Value;
                    break;
            }
        }

        private void btnDistributeEnergy_Click(object sender, EventArgs e)
        {
            DistributeEnergy();
        }

        private async void DistributeEnergy()
        {
            en1 = (double)nudEnergy.Value;
            en2 = (double)nudEnergy2.Value;

            await Distribute();
            cbShowEnergy.Checked = false;
            cbShowEnergy.Enabled = true;
            btnRSX.Enabled = true;
        }

        double en1 = 0;
        double en2 = 0;

        private bool hetero => _EnergyDistribution == EnergyDistribution.Heterogenous;

        private EnergyDistribution _EnergyDistribution;
        private Task Distribute()
        {
            if(hetero)
            {
                FindBoundaries();
            }
            double energyToAdd = en1;
            for(int i=0; i<x; i++)
            {
                for(int j=0; j<y; j++)
                {
                    if (hetero)
                    {
                        energyToAdd = (cells[i, j].Boundary) ? en1 : en2;
                    }

                    cells[i, j].Energy = RandFluctuation(energyToAdd);
                }
            }
            return Task.FromResult(true);
        }
        
        private double RandFluctuation(double energy)
        {
            var percent = r.Next(11);
            if(percent % 2 == 0)
            {
                percent *= -1;
            }
            var result = energy + (energy * percent / 100);
            return result > 0 ? result : 0;
        }

        private void DrawEnergy(bool cos = false)
        {
            if (InvokeRequired)
                Invoke(new DrawDelegate(DrawEnergy));
            else
            {
                GetColorsForEnergy(true);
                Cell cell = null;
                double energy = 0;
                bitmap = pb.Image as Bitmap;
                var newBitmap = pb.Image as Bitmap;
                for(int i=0; i<newBitmap.Width; i++)
                {
                    for(int j=0; j<newBitmap.Height; j++)
                    {
                        cell = cells[i, j];
                        energy = cell.Energy;
                        newBitmap.SetPixel(i, j, GetColorForEnergy(energy));
                    }
                }
                pb.Image = newBitmap;
            }
        }

        List<int> invalidIds = new List<int>(new int[] { -1, 0, SECOND_PHASE_ID });
        private void RecalculateEnergy()
        {
            for(int i=0; i<x; i++)
            {
                for(int j=0; j<y;j++)
                {
                    if (cells[i, j].Recrystalized || cells[i, j].Id < -1)
                        cells[i, j].Energy = 0;
                    else
                        cells[i, j].Energy = GetEnergy(cells[i, j].Id, GetNeighbours(cells[i, j], NeighborHood.MOORE).Where(c => c!=null).Select(c => c.Id).Except(invalidIds).ToList());
                }

            }
        }

        private int energyViewColors = 2;
        Dictionary<double, Color> colorList = new Dictionary<double, Color>();
        private Dictionary<double, Color> GetColorsForEnergy(bool recalculateEnergy = false)
        {
            int count = energyViewColors;
            colorList = new Dictionary<double, Color>();

            if (recalculateEnergy)
                RecalculateEnergy();

            var maxEn = cells.Cast<Cell>().Max(c => c.Energy);
            var dE = maxEn / count;
            int dColor = 255/count;
            for (int i=0; i<=count; i++)
            {
                double minEn = (i * dE);
                int c = (i * dColor);
                colorList.Add(minEn, Color.FromArgb(c, c, c));
            }
            return colorList;
        }

        private Color GetColorForEnergy(double energy)
        {
            var color = colorList.LastOrDefault(c => c.Key <= energy).Value;
            return color;
        }

        private void cbShowEnergy_CheckedChanged(object sender, EventArgs e)
        {
            nudEnergyColors.Enabled = label16.Enabled = cbShowEnergy.Checked;
            energyViewColors = (int)nudEnergyColors.Value;
            if(cbShowEnergy.Checked)
            {
                DrawEnergy();
            }
            else
            {
                nudEnergyColors.Value = 10;
                Draw();
            }
        }
        
        /*
         


         */

        private bool mcSeedDualPhase = false;
        private void cbMCDP_CheckedChanged(object sender, EventArgs e)
        {
            mcSeedDualPhase = cbMCDP.Checked;
        }


        private void cbEnergyDistribution_SelectedIndexChanged(object sender, EventArgs e)
        {
            _EnergyDistribution = (EnergyDistribution)cbEnergyDistribution.SelectedIndex;
            
            label22.Enabled = nudEnergy2.Enabled = hetero;

            string text = "\non boundaries";
            if(hetero)
            {
                label21.Text += text;
            }
            else
            {
                label21.Text = label21.Text.Replace(text, "");
            }
        }

        private SRXNucleation _SRXMode;
        private void cbRsx_SelectedIndexChanged(object sender, EventArgs e)
        {
            _SRXMode = (SRXNucleation)cbRsx.SelectedIndex;

            switch (_SRXMode)
            {
                case SRXNucleation.Constant:
                    {
                        label22.Enabled = nudRSXIncrease.Enabled = false;
                        break;
                    }
                case SRXNucleation.Increasing:
                    {
                        label22.Enabled = nudRSXIncrease.Enabled = true;
                        break;
                    }
                case SRXNucleation.Single:
                    {
                        label22.Enabled = nudRSXIncrease.Enabled = false;
                        break;
                    }
            }
        }

        private void nudEnergy_ValueChanged(object sender, EventArgs e)
        {
            CheckNudEnergy();
        }

        private void nudEnergy2_ValueChanged(object sender, EventArgs e)
        {
            CheckNudEnergy();
        }

        private void CheckNudEnergy()
        {
            if(hetero && nudEnergy.Value < nudEnergy2.Value)
            {
                MessageBox.Show("Boundaries energy is less than other cells energy.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void nudEnergyColors_ValueChanged(object sender, EventArgs e)
        {
            energyViewColors = (int)nudEnergyColors.Value;
            DrawEnergy();
        }

        private bool RSXEnable = false;
        private void cbRsxEnable_CheckedChanged(object sender, EventArgs e)
        {

        }

        private SRXLocation _SRXLocation;
        private void cbRSXLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            _SRXLocation = (SRXLocation)cbRSXLocation.SelectedIndex;
        }

        private bool _singleSRXDone = false;
        private int _RSXNucleonsToAdd = 0;
        private int _RSXNucleonsIncrease = 0;
        private void btnRSX_Click(object sender, EventArgs e)
        {
            nudRSXIterations_ValueChanged(null, null);
            for (int i = 0; i < _srxIterationsToMake; i++)
            {
                SingleSRX();
            }
        }

        private void SingleSRX()
        {
            _RSXNucleonsToAdd = (int)nudRSXNucleons.Value;
            _RSXNucleonsIncrease = (int)nudRSXIncrease.Value;
            SRXiterations++;
            textBox3.Text = SRXiterations.ToString();
            switch (_SRXMode)
            {
                case SRXNucleation.Constant:
                    {
                        DistributeRsxNucleons(_RSXNucleonsToAdd);
                        break;
                    }
                case SRXNucleation.Increasing:
                    {
                        _RSXNucleonsToAdd += _RSXNucleonsIncrease;
                        DistributeRsxNucleons(_RSXNucleonsToAdd);
                        break;
                    }
                case SRXNucleation.Single:
                    {
                        if (_singleSRXDone)
                        {
                            Draw();
                            break;
                        }

                        DistributeRsxNucleons(_RSXNucleonsToAdd);

                        _singleSRXDone = true;
                        break;
                    }
            }
            textBox4.Text = InvalidateSRXGrains().ToString();
            Task srxTask = SRX();
            srxTask.Start();

        }

        private int InvalidateSRXGrains()
        {
            var cellsList = cells.Cast<Cell>().Select(c => c?.Id).Where(c => c < -1).ToList();
            var missing = _recrystalizedIds.Where(i => !cellsList.Contains(i)).ToList();
            missing.ForEach(m =>
            {
                _recrystalizedIds.Remove(m);
                _srxColors.Remove(m);
            });

            return _recrystalizedIds.Where(i => cellsList.Contains(i)).Distinct().Count();
        }

        private int SRXiterations = 0;

        private void SRXIteration()
        {
            var cellList = new List<Cell>(cells.Cast<Cell>().Where(c => c.Id != SECOND_PHASE_ID && c.Id != -1).ToList());

            int count = cellList.Count();

            while(count > 0)
            {
                int index = r.Next(count);
                //index = 0;
                var cell = cellList[index];

                ProcessSRX(cell);

                cellList.RemoveAt(index);
                count = cellList.Count;
            }
            NewTimeStep(true);
        }

        private void ProcessSRX(Cell cell)
        {
            var nboors = GetNeighbours(cell, selectedHood);
            var rec = nboors.Where(c => c != null && c.Recrystalized).ToList();
            if(!rec.Any())
            {
                return;
            }
            int index = r.Next(rec.Count());
            var newId = rec[index].Id;
            var ids = nboors.Where(c => c != null).Select(c => c.Id).ToList();
           
            var energyBefore = GetEnergy(cell.Id, ids) + cell.Energy;

            var energyAfter = GetEnergy(newId, ids);

            var dE = energyAfter - energyBefore;

            if(dE <= 0)
            {
                Recristalize(cell.X, cell.Y, newId);
            }
        }

        private List<int> _recrystalizedIds = new List<int>();
        private int _baseRsxId = -2;

        private Dictionary<int, Color> _srxColors = new Dictionary<int, Color>();
        private int NewRsxId()
        {
            int newId = _baseRsxId;
            if (_recrystalizedIds.Any())
                newId = _recrystalizedIds.Min()-1;
            var c = GetUniqueColorSrx();
            _srxColors.Add(newId, c);
            _recrystalizedIds.Add(newId);
            return newId;
        }

        private void DistributeRsxNucleons(int count)
        {
            if(_SRXLocation == SRXLocation.GrainBoundaries)
            {
                FindBoundaries();
                for (int i = 0; i < count; i++)
                {
                    if (_recrystalizedIds.Count() > colorMaxValue - 2)
                        break;
                    var cell = GetRandomBoundaryCell();
                    Recristalize(cell.X, cell.Y, NewRsxId());
                }
            }
            else
            {
                var cellList = new List<Cell>(cells.Cast<Cell>().Where(c => c.Id != SECOND_PHASE_ID && c.Id >= 0 && !c.Recrystalized).ToList());

                for(int i=0; i<count; i++)
                {
                    if (_recrystalizedIds.Count() > 254)
                        break;

                    if (cellList.Count() == 0)
                        break;

                    int index = r.Next(cellList.Count());
                    var cell = cellList[index];
                    Recristalize(cell.X, cell.Y, NewRsxId());
                    cellList.RemoveAt(index);
                }
                NewTimeStep(true);
            }
            Draw();
        }

        private void Recristalize(int posx, int posy, int id)
        {
            cells[posx, posy].Energy = 0;
            cells[posx, posy].Recrystalized = true;
            cells[posx, posy].Id = id;
        }

        private Task SRX()
        {
            return new Task(() =>
            {
                    SRXIteration();
            });
        }
        
        private void nudJ_ValueChanged(object sender, EventArgs e)
        {
            J = (double)nudJ.Value;
        }

        private int _srxIterationsToMake = 0;
        private void nudRSXIterations_ValueChanged(object sender, EventArgs e)
        {
            _srxIterationsToMake = (int)nudRSXIterations.Value;
        }
    }

    public class Cell
    {
        public int Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public bool Boundary { get; set; }
        public double Energy { get; set; }
        public bool Recrystalized { get; set; }
    }
    
    public enum InclusionMode
    {
        None,
        Random,
        OnGrainBoundaries
    }

    public enum InclusionShape
    {
        SQUARE,
        CIRCULAR
    }

    public enum NeighborHood
    {
        MOORE,
        VONNEUMANN,
        DIFF
    }

    public enum StructureType
    {
        Substructure,
        DualPhase
    }

    public enum EnergyDistribution
    {
        Homogenous,
        Heterogenous
    }

    public enum SRXNucleation
    {
        Constant,
        Increasing,
        Single
    }

    public enum SRXLocation
    {
        GrainBoundaries,
        Anywhere
    }
}
